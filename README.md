# App Screenshots

Generate framed screenshots for Lee apps using `fastlane` and `frameit`.

## Setup

Requirements: MacOS, Xcode.

### Install Xcode command line tools

```bash
xcode-select --install
```

### Install fastlane

Install fastlane using [Homebrew](https://brew.sh/).

```bash
brew cask install fastlane
```

### Setup fastlane

Navigate your terminal to your project's directory and run

```bash
fastlane init
```

### Install ImageMagick

frameit depends on a tool called imagemagick to do image manipulation.

```bash
brew install libpng jpeg imagemagick
```


## Usage

Drop raw screenshots into the `screenshots` folder.

All iOS device sizes are supported. Image size will determine which device frame is used. 5.5 inch, 5.8 inch, and iPad is recommended.

Screenshots must be png format, and file names must include a brand identifier (`heraldreview`) and a content keyword (`home`, `nav`, or `article`).

Example filenames:

`5.5-heraldreview-home.png`  
`5.5-heraldreview-nav.png`  
`5.5-heraldreview-article.png`

`5.8-heraldreview-home.png`  
`5.8-heraldreview-nav.png`  
`5.8-heraldreview-article.png`

`ipad-heraldreview-home.png`  
`ipad-heraldreview-nav.png`  
`ipad-heraldreview-article.png`

Brand identifiers and keywords are defined in `Framefile.json` and determine the background images and text when generating framed screenshots.

Once screenshots are in place, navigate your terminal to the screenshots folder and run

```bash
fastlane frameit
```

## License
[MIT](https://choosealicense.com/licenses/mit/)